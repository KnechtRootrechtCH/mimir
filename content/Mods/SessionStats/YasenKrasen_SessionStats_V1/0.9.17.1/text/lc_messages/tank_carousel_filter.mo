��    3      �  G   L      h     i     y     �     �     �     �     �     �     �          !     .     H     ]     t     �     �     �     �     �     �          &     F     ]     x     �     �     �     �     �     �     �          ,     B     Z     k     ~     �     �     �     �     �     �          ,  !   G  #   i     �  M  �  3   �	     )
  /   H
     x
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                         $     B     G     L     S     b     j     o  I   �     �  P   �     0  +   ?     k     |     �  D   �     �     �       "     T   >     �  &   �     �     �     �  5        A  :   P     �      �     !          $   +       %              &                                                              (   #       ,   /                                            *            -   	   '         2      "   3           1          .            )   
   0    infotip/counter infotip/event infotip/header/description infotip/header/title infotip/levels infotip/nations infotip/only infotip/only/elite infotip/only/favorite infotip/only/premium infotip/rent infotip/searchNameVehicle infotip/vehicleTypes popover/checkbox/elite popover/checkbox/event popover/checkbox/favorite popover/checkbox/premium popover/checkbox/rented popover/counter popover/label/hidden popover/label/levels popover/label/nations popover/label/searchNameVehicle popover/label/specials popover/label/vehicleTypes popover/title tooltip/bonus/body tooltip/bonus/header tooltip/elite/body tooltip/elite/header tooltip/event/body tooltip/event/header tooltip/favorite/body tooltip/favorite/header tooltip/gameMode/body tooltip/gameMode/header tooltip/igr/body tooltip/igr/header tooltip/nations/body tooltip/params/body tooltip/params/header tooltip/premium/body tooltip/premium/header tooltip/rented/body tooltip/rented/header tooltip/searchInput/body tooltip/searchInput/header tooltip/toggleSwitchCarousel/body tooltip/toggleSwitchCarousel/header tooltip/vehicleTypes/body Project-Id-Version: World Of Tanks
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-06-01 10:00+0300
PO-Revision-Date: 2010-06-01 10:00+0300
Last-Translator: WoT Translator <noreply@wargaming.net>
Language-Team: WoT Team <noreply@wargaming.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Number of suitable vehicles: %(count)d of %(total)d Special vehicles are not shown Shows vehicles that meet the selected criteria. Vehicle Filter Tiers: Nations: Only the following vehicles: Elite Primary Premium Rental vehicles are not shown Search by name: Types: Elite Special Primary Premium Rental Displayed vehicles: %(count)s More Tier Nation Search by name Special Type Filter by Parameters Show/hide vehicles with the active x%(multiplier)s experience multiplier. Bonus Experience Show/hide vehicles with all available modules and technical branches researched. Elite Vehicles Show/hide vehicles for special game events. Special vehicles Show/hide primary vehicles. Primary Vehicles Check the box to view suitable vehicles for the %(battleType)s mode. Rampage Show/hide IGR vehicles. IGR Vehicles Show/hide vehicles of this nation. Configure the display of vehicles by their type, nation, tier, and other parameters. Filter by Parameters Show/hide Premium and reward vehicles. Premium Vehicles Show/hide rental vehicles. Rental vehicles Vehicle name must contain up to %(count)d characters. Search by name Switch between the one-level and two-level vehicle panels. Vehicle Panel Show/hide vehicles of this type. 